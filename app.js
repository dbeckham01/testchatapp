var bodyParser = require('body-parser')
var app = require('express')();
var request = require('request');
var http = require('http').Server(app);
var token = "CAAI7GC7PbloBADrAxEhZCg6tgBuOthRjknBNefzbtcvLe7Ec0sjEXIV67ffroZAZA1joRAx8KEOFdcjuXnrC7b5ohBDumhUVblZAabOYhZCGrxlpUUTcbaKm8fJ4IThsHgB52GUB9HCNjmyuA5vIXEZBQyDLD7ZCQwh07YeF3gIB50Wo6HznZCQlQ7jUifvaxT6yAGx1mZAziywZDZD";

var score = [0, 0, 0, 0, 0];
var totalScore = 0;

var emojiQLookup = [0,0,0];
var emotionAI = false;

var emojiLevel = 1;
//var emojiGold = 100;
var emojiGoldHolder = {};
var emojiHints = 3;

//objects to store profile details
var names = {};
var profilePics = {};

var hintBucket = [0,0,0];

//variables to hold online players.
var onlinePlayers = {};
var onlineCheckTime = 30*60; //5 seconds.

app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/', function(req, res){
  res.send('<h1>Hello world 5 gpv</h1>');
});

app.get('/webhook/', function (req, res) {
  if (req.query['hub.verify_token'] === 'check_this_token') {
    res.send(req.query['hub.challenge']);
  }
  res.send('Error, wrong validation token');
})

app.post('/webhook/', function (req, res) {
  messaging_events = req.body.entry[0].messaging;

  for (i = 0; i < messaging_events.length; i++) {
    event = req.body.entry[0].messaging[i];
    if(i>0) {
      prev_event = req.body.entry[0].messaging[i-1];
    }
    var friendId="1021127037975408";

    sender = event.sender.id;
    //marking this sender as online
    addOnlinePlayer(sender);
    //adding initial 100.
    if (!emojiGoldHolder.hasOwnProperty(sender)) {
      addGold(sender,100);
    }

    if (event.postback) {
      text = JSON.stringify(event.postback);
      console.log(text);
      //sendTextMessage(sender, "Postback received: "+text.substring(0, 200), token);
      if(text.substring(0,200) == "{\"payload\":\"You have successfully purchased 120 Pieces of Gold!\"}") {
        addGold(sender,120);
        sendTextMessage(sender, "You have successfully purchased 120 Pieces of Gold!", token);
      }
      if(text.substring(0,200) == "{\"payload\":\"You have successfully purchased 400 Pieces of Gold!\"}") {
        addGold(sender,400);
        sendTextMessage(sender, "You have successfully purchased 400 Pieces of Gold!", token);
      }
      if(text.substring(0,200) == "{\"payload\":\"You have successfully purchased 800 Pieces of Gold!\"}") {
        addGold(sender,800);
        sendTextMessage(sender, "You have successfully purchased 800 Pieces of Gold!", token);
      }
      if(text.substring(0,200) == "{\"payload\":\"A message has been sent to Friend #1\"}") {
        if(emojiQLookup[2] == 1 && emojiQLookup[1] == 1 && emojiQLookup[0] == 1) {
          if(hintBucket[0] == 0) {
              sendTextMessage(sender,"T _ _  _ o _ _  _ _ _ s _ _ _");
            sendTextMessage(friendId, "1. ⛰ ⛴");
              sendTextMessage(friendId,"T _ _  _ _ _ _  _ _ _ _ _ _ _");
          }
          if(hintBucket[1] == 0 && hintBucket[0] == 1) {
            sendTextMessage(sender,"T _ _  _ o _ _  _ _ _ _ _ _ _");
            sendTextMessage(friendId, "1. ⛰ ⛴");
            sendTextMessage(friendId,"T _ _  _ o _ _  _ _ _ _ _ _ _");
          }
          if(hintBucket[2]==0 && hintBucket[1]==1 && hintBucket[0]==1) {
            sendTextMessage(sender,"T _ _  _ _ _ _  _ _ _ _ _ _ _");
            sendTextMessage(friendId, "1. ⛰ ⛴");
            sendTextMessage(friendId,"T _ _  _ o _ _  _ _ _ s _ _ _");
          }

        }
        if(emojiQLookup[2] == 0 && emojiQLookup[1] == 1 && emojiQLookup[0] == 1) {
          if(hintBucket[0] == 0) {
              sendTextMessage(friendId, "2. ☝️✌️ >:( ");
              sendTextMessage(friendId,"T _ t _ n _ _");
          }
          if(hintBucket[1] == 0 && hintBucket[0] == 1) {
            sendTextMessage(friendId, "2. ☝️✌️ >:( ");
            sendTextMessage(friendId,"T _ t _ n _ _");
          }
          if(hintBucket[2]==0 && hintBucket[1]==1 && hintBucket[0]==1) {
            sendTextMessage(friendId, "2. ☝️✌️ >:( ");
            sendTextMessag(friendId,"T _ t _ n _ _");
          }

        }
        if(emojiQLookup[2] == 0 && emojiQLookup[1] == 0 && emojiQLookup[0] == 1) {
          if(hintBucket[0] == 0) {
            sendTextMessage(friendId, "3. 4 ☀️ ⚡️ ❄️");
              sendTextMessage(friendId,"T _ t _ n _ _");
          }
          if(hintBucket[1] == 0 && hintBucket[0] == 1) {
            sendTextMessage(friendId, "3. 4 ☀️ ⚡️ ❄️");
            sendTextMessage(friendId,"T _ t _ n _ _");
          }
          if(hintBucket[2]==0 && hintBucket[1]==1 && hintBucket[0]==1) {
            sendTextMessage(friendId, "3. 4 ☀️ ⚡️ ❄️");
            sendTextMessage(friendId,"T _ t _ n _ _");
          }

        }
      }
      if(text.substring(0,200) == "{\"payload\":\"A message has been sent to Friend #2\"}") {
        sendTextMessage(sender, "A request has been sent to Friend #2", token);
      }
      if(text.substring(0,200) == "{\"payload\":\"A message has been sent to Friend #3\"}") {
        sendTextMessage(sender, "A request has been sent to Friend #3", token);
      }
      if(text.substring(0,200) == "{\"payload\":\"You have sent Friend #1 a gift. You can send friends gifts every 8 hours :)\"}") {
        sendTextMessage(sender, "You have sent Friend #1 a gift. You can send friends gifts every 8 hours :)", token);
        addGold(friendId,3);
        sendTextMessage(friendId, "Your Friend has sent you Gold.Check your stats using @emojistats", token);

      }
      if(text.substring(0,200) == "{\"payload\":\"You have sent Friend #2 a gift. You can send friends gifts every 8 hours :)\"}") {
        sendTextMessage(sender, "You have sent Friend #2 a gift. You can send friends gifts every 8 hours :)", token);
        addGold(friendId,3);
        sendTextMessage(friendId, "Your Friend has sent you Gold.Check your stats using @emojistats", token);

      }
      if(text.substring(0,200) == "{\"payload\":\"You have sent Friend #3 a gift. You can send friends gifts every 8 hours :)\"}") {
        sendTextMessage(sender, "You have sent Friend #3 a gift. You can send friends gifts every 8 hours :)", token);
        addGold(friendId,3);
        sendTextMessage(friendId, "Your Friend has sent you Gold.Check your stats using @emojistats", token);

      }


      try{
        var payload = JSON.parse(event.postback.payload);
        friendId = payload.recipient;
        sendTextMessage(sender, "You have sent "+names[friendId] +" a gift. You can send friends gifts every 8 hours :)", token);
        addGold(friendId,3);
        sendTextMessage(friendId, "Your Friend has sent you Gold.Check your stats using @emojistats", token);
          
      }
      catch(e) {
        console.log(e);
      }


      continue;
    }

    if (event.message && event.message.text) {
      text = event.message.text;

      ///////////////////////////////////////////////////////////////////////////////////////////////////////////
      //Pop Quiz with flash cards
      //////////////////////////////////////////////////////////////////////////////////////////////////////////
      if (text === '@pop_quiz') {
        sendGenericMessage(sender);
        continue;
      }

      if(text === '@ans1:harry potter' && score[0] == 0) {
        score[0] = 1;
        totalScore = 0;
         for(scoreIndex=0; scoreIndex<score.length; scoreIndex++) {
            totalScore += score[scoreIndex];
        }
        sendTextMessage(sender, "That is correct! Score: "+totalScore+" :D");
      }

      if(text === '@ans2:super man' && score[1] == 0) {
        score[1] = 1;
        totalScore = 0;
         for(scoreIndex=0; scoreIndex<score.length; scoreIndex++) {
            totalScore += score[scoreIndex];
          }
        sendTextMessage(sender, "That is correct! Score: "+totalScore+" :D");
      }

      if(text === '@ans3:jasmine' && score[2] == 0) {
        score[2] = 1;
        totalScore = 0;
         for(scoreIndex=0; scoreIndex<score.length; scoreIndex++) {
            totalScore += score[scoreIndex];
          }
        sendTextMessage(sender, "That is correct! Score: "+totalScore+" :D");
      }

      //////////////////////////////////////////////////////////////////////////////////////////////////////////



      //////////////////////////////////////////////////////////////////////////////////////////////////////////
      //GuessEmoji
      //////////////////////////////////////////////////////////////////////////////////////////////////////////
      if(text=="@emojistart")
      {
          emojiLevel = 1;
          hintBucket = [0,0,0];
          emojiQLookup = [0,0,0];
          sendTextMessage(sender,"Welcome to Emoji✨ Type, '@next' for your first question. Good Luck! (y)");

      }

      if(text == '@emojibuy') {
        sendGenericMessageEmoji(sender);
        continue;
      }

      if(text == '@emojistats'){
          sendTextMessage(sender,"Your Level: "+emojiLevel+"\nYour Gold: "+getGold(sender)+" pieces");
      }

      if(text == '@emojihint') {
          if(hintBucket[0] == 0) {
              sendTextMessage(sender,"The 1st hint will cost you 10 Gold. Do you want to proceed? If you do type, '@emojihinty'.");
          }
          if(hintBucket[1] == 0 && hintBucket[0] == 1) {
            sendTextMessage(sender,"The 2nd hint will cost you 20 Gold. Do you want to proceed? If you do type, '@emojihinty'.");
          }
          if(hintBucket[2]==0 && hintBucket[1]==1 && hintBucket[0]==1) {
            sendTextMessage(sender,"The 3rd hint will cost you 30 Gold. Do you want to proceed? If you do type, '@emojihinty'.");
          }

      }

      if(text == '@emojibuddy') {

        sendGenericMessageBuddy(sender);
        continue;
      }

      if(text == '@emojihinty') {

        if((getGold(sender)-30 < 0) || (getGold(sender)-20 < 0) || (getGold(sender)-10 < 0)) {
          sendTextMessage(sender,"You don't have enough gold to buy a hint. '@emojibuy' to get some more.");
        }


        if(emojiQLookup[0] == 0) {

          if(hintBucket[2]==0 && hintBucket[1]==1 && hintBucket[0]==1) {
              hintBucket[2] = 1;
              addGold(sender,-30);
              sendTextMessage(sender,"T _ t _ n _ _");

              //hintBucket = [0,0,0];
          }

          if(hintBucket[1] == 0 && hintBucket[0] == 1) {
              hintBucket[1] = 1;
              addGold(sender,-20);
              sendTextMessage(sender,"_ _ t _ n _ _");
          }

          if(hintBucket[0] == 0) {
              hintBucket[0] = 1;
              addGold(sender,-10);
              sendTextMessage(sender,"_ _ t _ _ _ _");
          }
        }

        if(emojiQLookup[1] == 0 && emojiQLookup[0] == 1) {


          if(hintBucket[2]==0 && hintBucket[1]==1 && hintBucket[0]==1 && (getGold(sender)-30 >= 0)) {
              hintBucket[2] = 1;
              addGold(sender,-30);
              sendTextMessage(sender,"1 _  _ n _ r _  _ _ _");

              //hintBucket = [0,0,0];
          }

          if(hintBucket[1] == 0 && hintBucket[0] == 1) {
              hintBucket[1] = 1;
              addGold(sender,-20);
              sendTextMessage(sender,"_ _  _ n _ r _  _ _ _");
          }

          if(hintBucket[0] == 0) {
              hintBucket[0] = 1;
              addGold(sender,-10);
              sendTextMessage(sender,"_ _  _ n _ _ _  _ _ _");
          }
        }

        if(emojiQLookup[2] == 1 && emojiQLookup[1] == 1 && emojiQLookup[0] == 1) {


          if(hintBucket[2]==0 && hintBucket[1]==1 && hintBucket[0]==1 && (getGold(sender)-30 >= 0)) {
              hintBucket[2] = 1;
              addGold(sender,-30);
              sendTextMessage(sender,"T _ _  _ o _ _  _ _ _ s _ _ _");

              //hintBucket = [0,0,0];
          }

          if(hintBucket[1] == 0 && hintBucket[0] == 1 && (getGold(sender)-20 >= 0)) {
              hintBucket[1] = 1;
              addGold(sender,-20);
              sendTextMessage(sender,"T _ _  _ o _ _  _ _ _ _ _ _ _");
          }

          if(hintBucket[0] == 0 && (getGold(sender)-10 >= 0)) {
              hintBucket[0] = 1;
              addGold(sender,-10);
              sendTextMessage(sender,"T _ _  _ _ _ _  _ _ _ _ _ _ _");
          }
        }
      }




      if(text == '@next') {

        hintBucket = [0,0,0];

        if(emojiQLookup[0] == 0) {
          sendTextMessage(sender, "1. ⛰ ⛴");

        }

        if(emojiQLookup[1] == 0 && emojiQLookup[0] == 1) {
          sendTextMessage(sender, "2. ☝️✌️ >:( ");

        }

        if(emojiQLookup[2] == 0 && emojiQLookup[1] == 1 && emojiQLookup[0] == 1) {
          sendTextMessage(sender, "3. 4 ☀️ ⚡️ ❄️");

        }

        if(emojiQLookup[2] == 1 && emojiQLookup[1] == 1 && emojiQLookup[0] == 1) {
          sendTextMessage(sender, "Type, @emojistart to play again :)");

        }

      }

      if(text == ":)" || text == ":D" || text == ":P" || text == ":/" || text == "o.O" || text == ":O" || text == ";)") {
        sendTextMessage(sender, "You're quite emotive, I see :)");
        emotionAI = true;
      }

      if(text == ":(" || text == ":'(") {
        sendTextMessage(sender, "Don't give up! You've almost got it!");
        emotionAI = true;
      }

      if(text=="Titanic")
      {
        if(emojiQLookup[0] == 0 && emojiQLookup[1] == 0 && emojiQLookup[2] == 0) {
          emojiQLookup[0] = 1;
          emojiLevel ++;
          addGold(sender,5);
         sendTextMessage(sender, "That's correct! Good One! You have received 5 pieces of Gold :) Type, @emojistats to view your stats.\nType, @next for new questions. If you run into trouble, try @emojihelp");
       }
      }
      if(text=="12 angry men")
      {
        if(emojiQLookup[0] == 1 && emojiQLookup[1] == 0  && emojiQLookup[2] == 0) {
          emojiQLookup[1] = 1;
          emojiLevel ++;
          addGold(sender,5);
         sendTextMessage(sender, "Awesome! Right again! You have received 5 pieces of Gold :D");
       }
      }
      if(text=="The four seasons")
      {
        if(emojiQLookup[0] == 1 && emojiQLookup[1] == 1  && emojiQLookup[2] == 0) {
          emojiQLookup[2] = 1;
          emojiLevel ++;
          addGold(sender,5);
          sendTextMessage(sender, "That wasn't even easy. You're too good at this! :O You have received 5 pieces of Gold.");
          // clearing lookup table after the last question
          //emojiQLookup = [0,0,0];
       }
      }
      if(text == "@emojihelp") {

        sendTextMessage(sender, "If you're stuck and would like to use a hint, type '@emojihint' or use '@emojibuddy' to ask a friend for help.\nUse '@emojibuy' to visit the shop and buy gold. You'll find the greatest offers there!");
        sendTextMessage(sender, "There are 3 questions in total. Once you've answered a question correctly, type @next for a new question. \nEach time you answer a question correctly, you level up and earn 5 pieces of gold. You can always view your stats by calling '@emojistats'.");

      }
       if(text == "@myid") {

         sendTextMessage(sender, JSON.stringify(sender));
         sendTextMessage(1021127037975408, "Hey aroon");

      }
      else
      {
         // Handle a text message from this sender
         if(text!= "@emojistats"  && text!="@emojihint" && text!="@emojihinty" && text!="Titanic" && text!="12 angry men" && text!="The four seasons" && text!="@emojistart" && text!="@next" && text!="@emojihelp") {
            if(text!="" && text!=" " && text!=null && text.length!=0) {
              if(emotionAI == false) {
                sendTextMessage(sender, "Did you type that by accident? :)");

              }
              emotionAI = false;
            }
          }
      }

    }
  }
  res.sendStatus(200);
});



function sendTextMessage(sender, text) {
  messageData = {
    text:text
  }
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:sender},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}

function sendTextMessageBuddy(sender, text) {
  messageData = {
    text:text
  }
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages?access_token=CAAI7GC7PbloBADrAxEhZCg6tgBuOthRjknBNefzbtcvLe7Ec0sjEXIV67ffroZAZA1joRAx8KEOFdcjuXnrC7b5ohBDumhUVblZAabOYhZCGrxlpUUTcbaKm8fJ4IThsHgB52GUB9HCNjmyuA5vIXEZBQyDLD7ZCQwh07YeF3gIB50Wo6HznZCQlQ7jUifvaxT6yAGx1mZAziywZDZD',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:'1216659375'},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}

function sendGenericMessage(sender) {
  messageData = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "generic",
        "elements": [{
          "title": "Card #1",
          "image_url": "https://s-media-cache-ak0.pinimg.com/736x/56/57/82/5657828b0bb633aa36d5e264fa092a6d.jpg",

        },{
          "title": "Card #2",
          "image_url": "http://youngblah.com/wp-content/uploads/2012/06/minimalist-superhero-posters-iphone-wallpaper.jpg",
          },{
          "title": "Card #3",
          "image_url": "http://static.splashnology.com/articles/50_Fresh_Minimal_Movie_Posters/Aladdin_by_Leanne_Maher_02.png",
          }
          ]
      }
    }
  };
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:sender},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}

function sendGenericMessageEmoji(sender) {
  messageData = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "generic",
        "elements": [{
          "title": "Fistful of Gold",
          "subtitle": "120 Pieces",
          "image_url": "",
          "buttons": [ {
            "type": "postback",
            "title": "Rs. 100",
            "payload": "You have successfully purchased 120 Pieces of Gold!",
          }],
        },{
          "title": "Bucket of Gold",
          "subtitle": "400 Pieces",
          "image_url": "",
          "buttons": [ {
            "type": "postback",
            "title": "Rs. 300",
            "payload": "You have successfully purchased 400 Pieces of Gold!",
          }],
        },{
          "title": "Mountain of Gold",
          "subtitle": "800 Pieces",
          "image_url": "",
          "buttons": [ {
            "type": "postback",
            "title": "Rs. 500",
            "payload": "You have successfully purchased 800 Pieces of Gold!",
          }],
        }       ]
      }
    }
  };
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:sender},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}

function sendGenericMessageBuddy(sender) {
  var elements = [];
  var element = null;

  var giftPayload = null;
  for (var key in onlinePlayers) {
    if (onlinePlayers.hasOwnProperty(key)) {
      giftPayload = {type:"gift",recipient:key};
      element = {
          "title": "Friend #1",
          "subtitle": "Level ##",
          "image_url": "https://s-media-cache-ak0.pinimg.com/736x/0d/36/e7/0d36e7a476b06333d9fe9960572b66b9.jpg",
          "buttons": [ {
            "type": "postback",
            "title": "Ask for help",
            "payload": "A message has been sent to Friend #1",
          },{
            "type": "postback",
            "title": "Send Gift",
            "payload": JSON.stringify(giftPayload),
          }],
        };
      if(names.hasOwnProperty(key)) {
        element.title = names[key];
        element.image_url = profilePics[key];
      }
      elements.push(element);
    }
  }


  messageData = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "generic",
        "elements": elements
      }
    }
  };
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:sender},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}

//function to handle online players.
function addOnlinePlayer(senderId) {
  updateCache(senderId);
  onlinePlayers[senderId] = Date.now();
  removeNonActiveUsers();
  console.log(JSON.stringify(getOnlinePlayers()));

}

//function get the list of online players.
function getOnlinePlayers() {
  var senderIDs = [];
  removeNonActiveUsers();
  for (var key in onlinePlayers) {
    if (onlinePlayers.hasOwnProperty(key)) {
      senderIDs.push(key);
    }
  }
  return senderIDs;
}

//function to remove old users.
function removeNonActiveUsers() {
  var lastSeen = 0;
  for (var key in onlinePlayers) {
    if (onlinePlayers.hasOwnProperty(key)) {
      lastSeen = onlinePlayers[key];
      if((Date.now() - lastSeen) > onlineCheckTime * 1000) {
        delete onlinePlayers[key];
      }
    }
  }
}

function addGold(senderId,gold) {
  emojiGoldHolder[senderId] = Number(getGold(senderId) + gold);
  console.log(JSON.stringify(emojiGoldHolder));
}

function getGold(senderId) {
  if (emojiGoldHolder.hasOwnProperty(senderId)) {
    return Number(emojiGoldHolder[senderId]);
  }
  return 0;
}

function updateCache(senderId) {
  request({
    url: 'https://graph.facebook.com/v2.6/'+senderId,
    qs: {
      fields:'first_name,last_name,profile_pic',
      access_token:token},
    method: 'GET',
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
    else {
      processDetails(senderId,response.body);
    }
  });
}

function processDetails(senderId,response) {
  response = JSON.parse(response);
  names[senderId] = response.first_name + " " + response.last_name;
  profilePics[senderId] = response.profile_pic;
}

http.listen(8080,'10.128.19.173', function(){
  console.log('listening on *:8080');
});

